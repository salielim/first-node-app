// Load Express package
const express = require("express");

// Create instance of Express application by calling Express function
const app = express();

// Configure routes
    // Use public as directory for any files
    app.use(
        // Create handle for static content
        express.static(__dirname + "/public") //__dirname is current directory
    );

    // Show time on /time (have method & noun)
    app.get("/time", function(req, resp) {
        resp.status(200);
        resp.type("text/html");
        var curTime = new Date();
        resp.send(curTime.toISOString());
    });

    // Show 404 error (no method or noun)
    app.use(function(req, resp) {
        resp.status(404);
        resp.type("text/html");
        resp.send("404 error");
    })

// Start server
    // specify port
const port = 3000;

    // open port & execute function if successful
app.listen(
    port, // port to listen or bind to
    function() { //run this function
        console.log("application started on port %d", port);
    }
);